### *Python virtual env*
 

1. Git clone gruppe3 gitlab repo

2. Cd ind i det klonede gitlab REPO


```
cd gruppe-3
```

2. Opret Virtual  env med commando:

```
python -m venv env
```

3 . Aktiver virtual env:

Commando for Windows:
```
./env/Scripts/activate 
```

Commando for Linux: 
```
source ./env/bin/activate 
```

4. Install Python lib requirements med commando:

```
pip install -r requirements.txt
```

5. Naviger til main.py under src folder:

```
cd product/src/
```

5. Kør flask SOME app med commando:

```
flask --app main run --cert=adhoc
```


from os import urandom, path
from flask import Flask, render_template
from auth.authintication import authentication_controller
from User.user import user_controller
from database.dbconnector import db
from flask_login import login_required, current_user, LoginManager
from database.dbconnector import User
from flask_talisman import Talisman
from flask_wtf.csrf import CSRFProtect

# DB location/upload location, Turn on-off debug, secretkey
DATABASE = path.dirname(path.abspath(__file__)) + '/database/project.db'
UPLOAD_FOLDER = path.dirname(path.abspath(__file__)) + '/static/vid/'

DEBUG = False
SECRET_KEY = urandom(32)

# Setup for Flask app
app = Flask(__name__)
app.config['SECRET_KEY'] = SECRET_KEY
app.config['SQLALCHEMY_DATABASE_URI'] = "sqlite:///%s" % DATABASE
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
# files set to be no larger than 100 MB
app.config['MAX_CONTENT_LENGTH'] = 100 * 1000 * 1000
app.jinja_env.autoescape = True

Talisman(app)
db.init_app(app)
csrf = CSRFProtect(app)

login_manager = LoginManager()
login_manager.login_view = 'login'
login_manager.init_app(app)


# Routes and handlers for methods
@login_manager.user_loader
def load_user(id):
    return User.query.get(int(id))


@app.route("/")
def index():
    return render_template("index.html", user=current_user)


@app.route('/login', methods=['GET', 'POST'])
def login(self=None):
    login_handler = authentication_controller.login(self)
    return login_handler


@app.route('/create', methods=['GET', 'POST'])
def create(self=None):
    create_handler = authentication_controller.create(self)
    return create_handler


@app.route('/logout')
@login_required
def logout(self=None):
    logout_handler = authentication_controller.logout(self)
    return logout_handler


@app.route('/upload', methods=['GET', 'POST'])
@login_required
def upload(self=None):
    upload_handler = user_controller.upload(self, UPLOAD_FOLDER)
    return upload_handler


@app.route('/profile', methods=['GET'])
@login_required
def profile(self=None):
    profile_handler = user_controller.profile(self)
    return profile_handler


@app.route('/showvideo/<id>/', methods=['GET'])
@login_required
def show_video(id, self=None):
    show_video_handler = user_controller.show_video(self, id)
    return show_video_handler


@app.route('/sharevideo', methods=['POST'])
@login_required
def share_video(self=None):
    share_video_handler = user_controller.share_video(self)
    return share_video_handler


@app.route('/unshare', methods=['POST'])
@login_required
def unshare(self=None):
    unshare_video_handler = user_controller.unshare(self)
    return unshare_video_handler


@app.route('/add_comment', methods=['POST'])
@login_required
def add_comment(self=None):
    add_comment_handler = user_controller.add_comment(self)
    return add_comment_handler


@app.errorhandler(413)
def largefile_error(e):
    return render_template("413.html", user=current_user), 413


@app.errorhandler(404)
def page_not_found(e):
    return render_template("404.html", user=current_user), 404


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080, ssl_context=('adhoc'))

    # to use self-signed cert instead of dummy cert
    # create SSL key/cert pair and store in directory: cert. Then replace ssl_context above with:
    # (path.dirname(path.abspath(__file__)) + '/cert/cert.pem',
    #  path.dirname(path.abspath(__file__)) + '/cert/key.pem'))

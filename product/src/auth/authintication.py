from flask import request, redirect, url_for, \
    render_template, flash
from re import compile, match
from database.dbconnector import User, db
from flask_login import login_user, logout_user, current_user
from bcrypt import hashpw, checkpw, gensalt


class authentication_controller:

    def create(self):
        user_valid = None
        if request.method == 'POST':
            username = str(request.form['username'])
            password = str(request.form['password'])
            retyped_password = str(request.form['repassword'])
            user = User.query.filter_by(username=username).first()
            user_valid = authentication_controller.create_validation(self, user, username, password,
                                                                     retyped_password)
            if user_valid is True:
                hashed = hashpw(password.encode('utf-8'), gensalt())
                new_user = User(username=username, password=hashed)
                db.session.add(new_user)
                db.session.commit()
                flash('Successfully created - You can now login')
                return redirect(url_for('login'))

        return render_template('create.html', error=user_valid, user=current_user)

    def login(self):
        error = None
        if request.method == 'POST':
            username = str(request.form['username'])
            password = str(request.form['password'])
            user = User.query.filter_by(username=username).first()
            if user:
                if checkpw(password.encode('utf-8'), user.password):
                    login_user(user, remember=True)
                    flash('logged in successfully!', category='success')
                    return redirect(url_for('profile'))
                else:
                    error = "incorrect password"
            else:
                error = 'Invalid username or password'
        return render_template('login.html', error=error, user=current_user)

    def logout(self):
        logout_user()
        flash('You were logged out')
        return redirect(url_for('index'))

    def create_validation(self, user, username, password, retyped_password):
        username_regex = compile(r'[a-zA-Z0-9()$%_/.]{8,15}$')
        password_regex = compile(r'^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,20}$')
        if user:
            error = 'Username allready exist!'
        elif username == "":
            error = 'Username needed'
        elif not match(username_regex, username):
            error = 'Username should be between 8-15 chars and can contain mix of small and big letters'
        elif not match(password_regex, password):
            error = 'Password must be: Minimum eight characters, at least one letter, one number and one special character'
        elif password == "" or retyped_password == "":
            error = 'Password and retype password must filled out'
        elif password != retyped_password:
            error = 'Passwords you have typed does not match!'
        else:
            error = True

        return error

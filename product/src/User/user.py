from os import path
from flask import request, redirect, url_for, \
    render_template, flash
from database.dbconnector import db, User, Video, Share, Comments
from flask_login import current_user
from werkzeug.utils import secure_filename


class user_controller:

    def upload(self, uploadfolder):
        if request.method == 'POST':
            file = request.files['file']
            if not file:
                flash('No files was uploaded by current user', category='error')
                return render_template('upload.html', user=current_user)
            filename = secure_filename(file.filename)
            if not filename or not user_controller.allowed_file_extension(self, filename):
                flash("Only ext. these extensions can be uploaded 'mp4', 'mp5', 'mkv', 'mpv', 'webm'", category="error")
                return render_template('upload.html', user=current_user)

            video_to_upload = Video(filename=filename, user_id=current_user.id)
            db.session.add(video_to_upload)
            db.session.commit()
            file.save(path.join(uploadfolder, filename))
            flash('Video was successfully uploaded!')

        return render_template('upload.html', user=current_user)

    def share_video(self):
        if request.method == 'POST':
            video_id = request.form['vidId']
            to_userid = request.form['userid']
            shared_filename = db.session.query(Video.filename).filter(Video.id == video_id).scalar_subquery()
            new_share = Share(video_id=video_id, from_id=current_user.id, to_id=to_userid, filename=shared_filename)
            db.session.add(new_share)
            db.session.commit()
            flash('Video has been shared!', category='success')
            return redirect(url_for('profile'))

    #
    def unshare(self):
        if request.method == 'POST':
            video_id = request.form.get('videoid')
            shared_id = request.form.get('shareduser', None)
            if shared_id is None:
                flash("Must select user to unshare")
                return redirect(url_for('show_video', id=video_id))
            Share.query.filter(Share.to_id == shared_id and Share.video_id == video_id).delete()
            db.session.commit()
            flash("Video has been unshared")
            return redirect(url_for('profile'))

    def add_comment(self):
        if request.method == 'POST':
            video_id = request.form['videoid']
            comment = request.form['text']
            new_comment = Comments(data=comment, user_id=current_user.id, video_id=video_id)
            db.session.add(new_comment)
            db.session.commit()
            flash('Added comment!')
            return redirect(url_for('show_video', id=video_id))

    #
    def profile(self):
        return render_template('profile.html', user=current_user)

    #
    def show_video(self, id):
        usr = User.query.all()
        all_share = Share.query.all()
        vid = Video.query.filter_by(id=id).first()

        if not vid:
            return render_template('video_not_found.html', user=current_user), 404
        return render_template("video.html", usernames=usr, user=current_user, vid=vid,
                               owner=vid.user_id == current_user.id, shares=all_share)

    def allowed_file_extension(self, filename):
        allowed_extensions = {'mp4', 'mp5', 'mkv', 'mpv', 'webm'}
        return '.' in filename and \
               filename.rsplit('.', 1)[1].lower() in allowed_extensions

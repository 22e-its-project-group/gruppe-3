import unittest
from auth import authintication

# opretter et object af authintication_controller, så vi kan bruge "create_validation" metoden
auth = authintication.authentication_controller()


class TestValidUserCreateMethod(unittest.TestCase):

    # Test for an actually valid user
    def test_valid_user(self):
        user = False
        username = "TestoBoyen"
        password = "Easycrack123#"
        retype_password = "Easycrack123#"
        self.assertTrue(auth.create_validation(user, username, password, retype_password))

    # Test is the user is allready exist in DB
    def test_user_exist(self):
        user = True
        username = "TestoBoyen"
        password = "Easycrack123#"
        retype_password = "Easycrack123#"
        self.assertEqual(auth.create_validation(user, username, password, retype_password),
                         'Username allready exist!')

    # Test if username was not typed
    def test_username_needed(self):
        user = False
        username = ""
        password = "Easycrack123#"
        retype_password = "Easycrack123#"
        self.assertEqual(auth.create_validation(user, username, password, retype_password), "Username needed")

    # Test if username passer the regex required
    def test_username_regex(self):
        user = False
        username = "Testob"
        password = "Easycrack123#"
        retype_password = "Easycrack123#"
        self.assertEqual(auth.create_validation(user, username, password, retype_password),
                         "Username should be between 8-15 chars and can contain mix of small and big letters")

    # Test if password passed the regex required
    def test_password_regex(self):
        user = False
        username = "TestoBoyen"
        password = "Easycrack123"
        retype_password = "Easycrack123"
        self.assertEqual(auth.create_validation(user, username, password, retype_password),
                         "Password must be: Minimum eight characters, at least one letter, one number and one special character")

    # Test if password is retyped
    def test_password_retype(self):
        user = False
        username = "TestoBoyen"
        password = "Easycrack123#"
        retype_password = ""
        self.assertEqual(auth.create_validation(user, username, password, retype_password),
                         'Password and retype password must filled out')

    # Test with were password dont match
    def test_password_match(self):
        user = False
        username = "TestoBoyen"
        password = "Easycrack123#"
        retype_password = "Easycrack123"
        self.assertEqual(auth.create_validation(user, username, password, retype_password),
                         'Passwords you have typed does not match!')


if __name__ == '__main__':
    unittest.main()

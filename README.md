# Gruppe 3


### Af: Sune, Oscar, Ulrik


## opsætning af programmet

Der bruges docker til at køre SOME APP

### *Docker opsætnings  guide*



1. Herefter naviger til mappen src med CD

```
cd <Stien til src>
```

2. Nu vil næste skridt være at opbygge et docker image ved brug af Dockerfile og entryPoint. Her skal target repository være der hvor de to filer ligger (src):

```
docker build -t src .

```
3. Til sidst opsættes der en docker container med det nye image.

```
docker run -d -p 8080:8080 src

```
4. Åben browser og tilgå siden med https og port 8080

[https://127.0.0.1:8080](https://127.0.0.1:8080)




